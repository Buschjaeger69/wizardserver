import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.plaf.synth.SynthSpinnerUI;

public class Server {

	private int port;
	private int maxSpielerzahl;
	private List<Mitspieler> mitspieler;
	private boolean istBeschaeftigt;

	public Server(int port, int maxSpielerzahl) {
		super();
		this.port = port;
		this.maxSpielerzahl = maxSpielerzahl;
		mitspieler = new ArrayList<Mitspieler>();
		istBeschaeftigt = true;
		acceptClients();
	}

	public List<Mitspieler> getMitspieler() {
		return mitspieler;
	}

	public void setMitspieler(List<Mitspieler> mitspieler) {
		this.mitspieler = mitspieler;
	}

	public boolean isIstBeschaeftigt() {
		return istBeschaeftigt;
	}

	public void setIstBeschaeftigt(boolean istBeschaeftigt) {
		this.istBeschaeftigt = istBeschaeftigt;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getMaxSpielerzahl() {
		return maxSpielerzahl;
	}

	public void setMaxSpielerzahl(int maxSpielerzahl) {
		this.maxSpielerzahl = maxSpielerzahl;
	}

	public void addMitspieler(Mitspieler m) {
		mitspieler.add(m);
	}

	public void acceptClients() {
		new Thread(new Runnable() {

			public void run() {
				ServerSocket ss;
				try {
					ss = new ServerSocket(port);

					boolean weiter = true;
					while (weiter) {
						try {
							System.out.println("Port-----------------" + port);

							Socket client = ss.accept();
							BufferedReader is = new BufferedReader(new InputStreamReader(client.getInputStream()));
							PrintWriter os = new PrintWriter(new PrintStream(client.getOutputStream()), true);

							String benutzername = is.readLine();
							Mitspieler m = new Mitspieler(mitspieler.size(), client, benutzername, is, os);
							mitspieler.add(m);
							System.out.println("Client angenommen: ");
							System.out.println(m);
							if (mitspieler.size() == maxSpielerzahl) {
								System.out.println("Spiel starten");
								spielStarten();
								weiter = false;
							}
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}).start();
	}

	public void spielStarten() {
		System.out.println("Spiel gestartet");
		System.out.println("Mitspielerliste: ");

		// an alle Mitspieler ein Startsignal senden
		for (Mitspieler mitspieler2 : mitspieler) {
			System.out.println(mitspieler2);
			mitspieler2.getOs().println("start");
		}

		try {
			for (int i = 0; i < mitspieler.size(); i++) {
				System.out.println("sinnlos1");
				mitspieler.get(i).getOs().println("1");
				for (int j = 0; j < mitspieler.size(); j++) {
					System.out.println("sinnlos2");
					mitspieler.get(i).getOs().println(mitspieler.get(j).getName());

				}
				System.out.println("---------------------------");
			}
			spielAblauf();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendMitspielerliste() {
		// An alle Mitspieler der Liste die Namen der anderen wartenden Mitspieler
		// senden
		for (Mitspieler mitspieler2 : mitspieler) {
			for (Mitspieler mitspieler : mitspieler) {
				mitspieler2.getOs().println(mitspieler.getName());
			}

		}
	}

	@Override
	public String toString() {
		return "Server [port=" + port + ", maxSpielerzahl=" + maxSpielerzahl + ", mitspieler=" + mitspieler + "]";
	}

	public void spielAblauf() throws NumberFormatException, IOException {
		// Vorbereitung vor dem Start
		System.out.println("Start");
		// Kartenarray erzeugen + vorbelegen
		int[] kartenarray = kartenFeldErzeugen();

		// Karten mischen
		KartenFeldMischen(kartenarray);

		// Spielstart

		// Rundenzahl variiert je nach Spielerzahl
		int maxrundenzahl = 60 / maxSpielerzahl;
		// dies ist die Position des ersten Spielers
		int stapelPos = 0;
		// Anzahl der Mitspieler
		int mitspielerzahl = mitspieler.size();
		// Nach jeder Runde
		boolean ersteKartenRunde = true;

		// Hier werden die Stichzahlen abgespeichert
		int[] stichzahl = new int[maxSpielerzahl];

		// Spieler, der den letzten Stich gemacht hat
		int stichspieler = 0;

		// Hier werden die Punkte der Mitspieler abgespeichert
		int[] punktzahl = new int[maxSpielerzahl];

		int[] angesagteStiche = new int[maxSpielerzahl];

		// 10 oder mehr Runden
		for (int i = 0; i < maxrundenzahl; i++) {
			// Z�hler f�r aktuelle rundenzahl
			int aktrundenzahl = i + 1;

			Mitspieler[] spieler = new Mitspieler[mitspielerzahl];

			int[] mitspielerarr = new int[mitspielerzahl];
			int[] ergebnis = new int[mitspielerzahl];

			System.err.println("stichspieler: " + stichspieler);

			for (int u = 0; u < ergebnis.length; u++) {
				mitspielerarr[u] = u;
			}

			//Beginn Reihenfolgenermittlung
			
				ergebnis[0] = stapelPos;
			
			System.err.println("Ergebnis: "+ergebnis[0]);
			
			int index = 0;
			for (int k = 0; k < ergebnis.length; k++) {
				index = ergebnis[0] + k;
				if (index >= ergebnis.length) {
					index = index - ergebnis.length;
				}
				ergebnis[k] = mitspielerarr[index];
			}

			for (int j = 0; j < ergebnis.length; j++) {
				spieler[j] = mitspieler.get(ergebnis[j]);
			}
			//Ende Reihenfolgenermittlung
			
			Mitspieler [] temppunkteberechnung= new Mitspieler [spieler.length];
			for (int j = 0; j < spieler.length; j++) {
				temppunkteberechnung[j]=spieler[j];
			}
			
			
			// Karten austeilen

			System.out.println("Test");

			System.out.println("***Reihenfolge***");
			for (int j = 0; j < spieler.length; j++) {
				System.out.println(spieler[j].getName());
			}

			// Karten je nach aktueller Runde an Mitspieler austeilen
			for (int j = 0; j < spieler.length; j++) {
				if (spieler[j].getOs() != null) {
					System.out.println("Karten an Mitspieler austeilen");
					spieler[j].getOs().println("3");
					System.out.println("Karte an Client austeilen: " + spieler[j]);
					kartenAusteilen(kartenarray, aktrundenzahl, spieler[j].getOs());
				} else {
					System.out.println("Spieler �bersprungen");
				}

			}

			System.out.println("Abwarten auf Stichansage");
			int [] zwischenangesagteStiche = new int[maxSpielerzahl];
			
			// Stichansage der Spieler abwarten
			for (int j = 0; j < spieler.length; j++) {
				if (spieler[j].getIs() != null && spieler[j].getOs() != null) {
					spielerBenachrichtigen(spieler[j].getMitspielernummer(), mitspieler);
					// Stichansage der Spieler
					System.out.println("Stichansage der Spieler");
					spieler[j].getOs().println("2");
					System.out.println("Waiting...");
					
					angesagteStiche[j] = Integer.parseInt(spieler[j].getIs().readLine());
					System.err.println("angesagter Stich: "+angesagteStiche[j]+"|"+spieler[j]);
					System.out.println("Erfolg");
				} else {
					System.out.println("Spieler �bersprungen");
				}

			}
			
			
			
			

			// erst Liste der Spieler nach Mitspielernummern sortieren
			int[] sorted = new int[spieler.length];

			for (int j = 0; j < sorted.length; j++) {
				sorted[spieler[j].getMitspielernummer()] = angesagteStiche[j];
			}

			// Stichansagen an alle Spieler �bertragen
			for (int j = 0; j < sorted.length; j++) {
				if (spieler[j].getOs() != null) {
					System.out.println("Stichansagen an Spieler �bertragen");
					spieler[j].getOs().println("4");

					for (int j2 = 0; j2 < sorted.length; j2++) {
						if (spieler[j].getOs() != null) {
							spieler[j].getOs().println(sorted[j2]);
						}
					}
				}
			}

			// Karten legen bis die Runde vorbei ist
			for (int j = 0; j < aktrundenzahl; j++) {
				
				if(j>0) {
					ersteKartenRunde=false;
				}
				
				System.err.println("Wert ersteKartenRunde: "+ersteKartenRunde);
				//Beginn
				if (ersteKartenRunde) {
					
				} else {
					System.err.println("This is the right way");
					ergebnis[0] = stichspieler;
					int ind = 0;
					for (int k = 0; k < ergebnis.length; k++) {
						ind = ergebnis[0] + k;
						if (ind >= ergebnis.length) {
							ind = ind - ergebnis.length;
						}
						ergebnis[k] = mitspielerarr[ind];
					}

					for (int z = 0; z < ergebnis.length; z++) {
						spieler[z] = mitspieler.get(ergebnis[z]);
					}
				}
				System.err.println("Ergebnis: "+ergebnis[0]);
				
				
				
				
				
				//Ende
				
				int hoechsteKarte = -1;
				// Alle Spieler m�ssen eine Karte legen
				for (int j2 = 0; j2 < spieler.length; j2++) {
					boolean neueHoechsteKarte = false;

					// Dem Spieler sagen, dass er eine Karte spielen muss
					System.out.println("Reihenfolger der Spieler");
					for (int k = 0; k < spieler.length; k++) {
						System.out.println(spieler[k].getName());
						System.out.println(spieler[k].getMitspielernummer());
					}
					if (spieler[j2].getOs() != null) {
						System.out.println("Spieler benachrichtigen, dass er eine Karte spielen muss");
						spieler[j2].getOs().println("8");
						spielerBenachrichtigen(spieler[j2].getMitspielernummer(), mitspieler);

						// gespielte Karte auslesen
						int gespielteKarte = Integer.parseInt(spieler[j2].getIs().readLine());

						for (int k = 0; k < spieler.length; k++) {
							if (spieler[k].getOs() != null) {
								System.out.println("gespielte Karte an Spieler �bertragen");
								spieler[k].getOs().println("13");
								spieler[k].getOs().println(gespielteKarte);
							}
						}
						int vergleich = hoehereKarte(hoechsteKarte, gespielteKarte);

						if (vergleich == 1) {
							hoechsteKarte = gespielteKarte;
							//Hier weiter
							stichspieler = spieler[j2].getMitspielernummer();
							neueHoechsteKarte = true;
						}
						if (neueHoechsteKarte) {
							for (int k = 0; k < spieler.length; k++) {
								if (spieler[k].getOs() != null) {
									System.out.println("Neue h�here Karte an Mitspieler schicken");
									System.out.println("Wert: " + hoechsteKarte);
									spieler[k].getOs().println("5");
									spieler[k].getOs().println(hoechsteKarte);
								}
							}
						}
					}

				}
				System.err.println("Stichspieler: "+stichspieler);
				for (int k = 0; k < spieler.length; k++) {
					// Hier weiter
					int stichausgabe;
					mitspieler.get(k).getOs().println("6");
					if (k == stichspieler) {
						stichausgabe = 1;

					} else {
						stichausgabe = 0;
					}
					System.err.println(mitspieler.get(k).getName());
					System.err.println("gemachte Stiche: " + stichausgabe);

					mitspieler.get(k).getOs().println(stichausgabe);
				}

				// for (int k = 0; k < spieler.length; k++) {
				// int stichausgabe;
				// spieler[k].getOs().println("6");
				// if(k==spieler[stichspieler].getMitspielernummer()){
				// System.out.println("****************Spieler
				// "+spieler[stichspieler].getName()+" hat gestochen"+k);
				// stichausgabe = 1;
				// stichzahl[k]++;
				// }else {
				// stichausgabe = 0;
				// }
				// System.out.println(spieler[k].getName());
				// System.out.println("gemachte Stiche: "+stichausgabe);
				//
				// spieler[k].getOs().println(stichausgabe);
				// }

				for (int k = 0; k < mitspieler.size(); k++) {
					System.out.println("H�chste Karte auf 0 setzen");

					mitspieler.get(k).getOs().println("5");
					mitspieler.get(k).getOs().flush();
					mitspieler.get(k).getOs().println("0");
					mitspieler.get(k).getOs().flush();
					System.out.println("Stichgewinner an alle schicken");
					System.out.println("Wert stichspieler: " + stichspieler);
					mitspieler.get(k).getOs().println("14");
					mitspieler.get(k).getOs().flush();
					mitspieler.get(k).getOs().println(stichspieler);
					mitspieler.get(k).getOs().flush();
				}
				stichzahl[stichspieler]++;

			}
			System.out.println("Runde ist vorbei, weiter zur n�chsten Runde");
			kartenarray = kartenFeldErzeugen();
			KartenFeldMischen(kartenarray);
			ersteKartenRunde = true;
			

			System.err.println("angesagte Stiche in falscher Reihenfolge: ");
			for (int j = 0; j < zwischenangesagteStiche.length; j++) {
				System.err.println(spieler[j].getName()+"|"+temppunkteberechnung[j].getMitspielernummer());
				System.err.println(angesagteStiche[j]);
				
			}
			
			
			for (int j = 0; j < angesagteStiche.length; j++) {
				int temp = temppunkteberechnung[j].getMitspielernummer();
				System.err.println("Temp: "+temp);
				zwischenangesagteStiche[temp]=angesagteStiche[j];
			}
			
			System.err.println("angesagte Stiche in richtiger Reihenfolge: ");
			for (int j = 0; j < zwischenangesagteStiche.length; j++) {
				System.err.println(zwischenangesagteStiche[j]);
			}
			
			System.err.println("Beginn Punkteberechnung");
			for (int j = 0; j < sorted.length; j++) {
				System.err.println("Name: "+mitspieler.get(j).getName());
				System.err.println("angesagteStiche: "+zwischenangesagteStiche[j]);
				System.err.println("gemachteStiche: "+stichzahl[j]);
				System.err.println("errechnete Punktzahl: "+PunkteBerechnung(zwischenangesagteStiche[j], stichzahl[j]));
				punktzahl[j] += PunkteBerechnung(zwischenangesagteStiche[j], stichzahl[j]);
				System.err.println("aktuelle Punktzahl: ");
				System.err.println("----------------------------------");
			}
			System.err.println("Ende Punkteberechnung");
			
			for (int j = 0; j < sorted.length; j++) {
				System.out.println("Punktezahl der Runde an die Mitspieler schicken");
				mitspieler.get(j).getOs().println(7);
				for (int j2 = 0; j2 < sorted.length; j2++) {
					mitspieler.get(j).getOs().println(punktzahl[j2]);
				}
			}

			// cleanup f�r n�chste Runde
			stapelPos++;

			if (stapelPos == mitspielerzahl) {
				stapelPos = 0;
			}
			System.out.println("Cleanup f�r n�chste Runde");
			for (int j = 0; j < stichzahl.length; j++) {
				stichzahl[j] = 0;
			}
			System.out.println("Beginn n�chste Runde");

		}

		// Ermittlung des Gewinners

		int max = -99999;
		int index = 0;
		for (int i = 0; i < mitspieler.size(); i++) {
			if (punktzahl[i] > max) {
				max = punktzahl[i];
				index = i;
			}

		}

		for (int i = 0; i < angesagteStiche.length; i++) {
			mitspieler.get(i).getOs().println(11);
			mitspieler.get(i).getOs().println(index);

			for (int j = 0; j < angesagteStiche.length; j++) {
				mitspieler.get(i).getOs().println(punktzahl[j]);
			}
		}

	}

	public int PunkteBerechnung(int ansage, int gemacht) {
		int punkte = 0;
		if (ansage == gemacht) {
			punkte = 20 + (ansage * 10);
		} else {
			punkte = Math.abs(ansage - gemacht) * (-10);
		}
		return punkte;
	}

	public void kartenAusteilen(int[] kartenarray, int rz, PrintWriter os) {

		for (int i = 0; i < rz; i++) {
			int karte = karteZiehen(kartenarray);
			os.println(karte);
			System.out.println("Karte ausgeteilt: " + karte);
		}
	}

	public int karteZiehen(int[] kartenarray) {
		for (int i = 0; i < kartenarray.length; i++) {
			if (kartenarray[i] != -1) {
				int temp = kartenarray[i];
				kartenarray[i] = -1;
				return temp;
			}
		}
		return -1;
	}

	public int[] kartenFeldErzeugen() {
		int[] kartenarray = new int[60];
		for (int i = 0; i < 60; i++) {
			kartenarray[i] = i + 1;
		}
		return kartenarray;
	}

	public void KartenFeldMischen(int[] kartenarray) {
		Random r = new Random();
		for (int i = kartenarray.length - 1; i > 0; i--) {

			int random = r.nextInt(i + 1);
			int tmp = kartenarray[random];
			kartenarray[random] = kartenarray[i];
			kartenarray[i] = tmp;
		}

	}

	public void spielerBenachrichtigen(int spielerid, List<Mitspieler> mitspieler) {
		for (int i = 0; i < mitspieler.size(); i++) {
			mitspieler.get(i).getOs().println("10");
			mitspieler.get(i).getOs().println(spielerid);
		}
	}

	public int hoehereKarte(int mittlereKarte, int gelegteKarte) {
		int hoeher;
		if (mittlereKarte == -1) {
			return 1;
		}

		int[] werte = kartenzuordnung(mittlereKarte);
		int gruppe1 = werte[0];
		int kartenwert1 = werte[1];
		int farbgruppe1 = werte[2];

		werte = kartenzuordnung(gelegteKarte);
		int gruppe2 = werte[0];
		int kartenwert2 = werte[1];
		int farbgruppe2 = werte[2];

		if (kartenwert1 == 0) {
			hoeher = 1;
		}
		if (gruppe1 < 3) {
			if (gruppe1 == 1) {
				if (gruppe2 == 1) {
					hoeher = 0;
				} else {
					hoeher = 1;
				}
			} else {

				if (gruppe2 == 3) {
					hoeher = 1;
				} else {
					if (gruppe2 == 1) {
						hoeher = 0;
					} else {

						if (farbgruppe2 == farbgruppe1) {
							if (kartenwert2 > kartenwert1) {
								hoeher = 1;
							} else {
								hoeher = 0;
							}
						} else {
							hoeher = 0;
						}
					}
				}
			}

		} else {
			hoeher = 0;
		}

		return hoeher;
	}

	public int[] kartenzuordnung(int kartennr) {
		int[] werte = new int[3];
		int gruppe;
		int kartenwert;
		int farbgruppe;
		if (kartennr == 0) {
			gruppe = 0;
			kartenwert = 0;
			farbgruppe = 0;
		}
		if (kartennr <= 4) {
			gruppe = 3;
			kartenwert = 14;
			farbgruppe = 0;
		} else {
			if (kartennr <= 8) {
				gruppe = 1;
				kartenwert = 0;
				farbgruppe = 0;
			} else {
				if (kartennr <= 21) {
					gruppe = 2;
					kartenwert = kartennr - 8;
					farbgruppe = 1;
				} else {
					if (kartennr <= 34) {
						gruppe = 2;
						kartenwert = kartennr - 21;
						farbgruppe = 2;
					} else {
						if (kartennr <= 47) {
							gruppe = 2;
							kartenwert = kartennr - 34;
							farbgruppe = 3;
						} else {
							gruppe = 2;
							kartenwert = kartennr - 47;
							farbgruppe = 4;
						}
					}

				}
			}
		}

		werte[0] = gruppe;
		werte[1] = kartenwert;
		werte[2] = farbgruppe;
		return werte;
	}

	private Mitspieler findSpieler(String name) {
		for (int i = 0; i < mitspieler.size(); i++) {
			if(mitspieler.get(i).getName().equals(name)){
				return mitspieler.get(i);
			}
		}
		return null;
	}
	
}
