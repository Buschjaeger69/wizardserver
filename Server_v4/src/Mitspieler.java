import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Mitspieler {
	private int mitspielernummer;
	private Socket socket;
	private String name;
	private BufferedReader is;
	private PrintWriter os;
	
	//TODO evtl. Threads verwenden
	
	public Mitspieler(int mitspielernummer, Socket socket, String name, BufferedReader is, PrintWriter os) {
		super();
		this.mitspielernummer = mitspielernummer;
		this.socket = socket;
		this.name = name;
		this.is = is;
		this.os = os;
		
	}

	
	
    
	@Override
	public String toString() {
		return "Mitspieler [mitspielernummer=" + mitspielernummer + ", socket=" + socket + ", name=" + name + ", is="
				+ is + ", os=" + os + "]";
	}


	public int getMitspielernummer() {
		return mitspielernummer;
	}


	public void setMitspielernummer(int mitspielernummer) {
		this.mitspielernummer = mitspielernummer;
	}


	public Socket getSocket() {
		return socket;
	}


	public void setSocket(Socket socket) {
		this.socket = socket;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public BufferedReader getIs() {
		return is;
	}


	public void setIs(BufferedReader is) {
		this.is = is;
	}


	public PrintWriter getOs() {
		return os;
	}


	public void setOs(PrintWriter os) {
		this.os = os;
	}
	
	
	
	
}
